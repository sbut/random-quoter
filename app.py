# app.py - a minimal flask api using flask_restful
from random import choice
from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class RandomQuoter(Resource):
    def get(self):
        f = open("quotes.txt", "r")
        quotes_file = f.read()
        quotes = quotes_file.split("\n\n")

        return { "quote": "{}".format(choice(quotes)) }

api.add_resource(RandomQuoter, '/')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
